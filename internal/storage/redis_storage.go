package storage

import (
	"github.com/garyburd/redigo/redis"
	"gitlab.com/rtemb/catosniff/internal/config"
)

type RedisStorage struct {
	pool *redis.Pool
}

var _ Storage = (*RedisStorage)(nil)

func NewStorage(cfg *config.Redis) *RedisStorage {
	redisPool := &redis.Pool{
		MaxIdle:     cfg.MaxIdle,
		IdleTimeout: cfg.IdleTimeout,
		Dial: func() (redis.Conn, error) {
			opts := []redis.DialOption{
				redis.DialDatabase(0),
			}

			if cfg.Password != "" {
				opts = append(opts, redis.DialPassword(cfg.Password))
			}
			return redis.Dial("tcp", cfg.Address, opts...)
		},
	}

	return &RedisStorage{pool: redisPool}
}

func (s *RedisStorage) GetQuestion() (string, error) {
	return "", nil
}

func (s *RedisStorage) SetQuestion(q string) error {
	return nil
}
