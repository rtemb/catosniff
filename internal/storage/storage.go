package storage

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 --fake-name StorageMock -o ../testing/mocks/storage.go . Storage
type Storage interface {
	Getter
}

type Getter interface {
	GetQuestion() (string, error)
}

type Setter interface {
	SetQuestion(q string) error
}
