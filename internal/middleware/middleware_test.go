package middleware_test

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/sirupsen/logrus"
	"github.com/sirupsen/logrus/hooks/test"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	mw "gitlab.com/rtemb/catosniff/internal/middleware"
	"gitlab.com/rtemb/catosniff/internal/testing/mocks"
)

func TestRequestMiddleware_LogRequests(t *testing.T) {
	handler, hook, _, err := initHandler()
	require.NoError(t, err)

	req, err := http.NewRequest("GET", "/v1/api/healthcheck", nil)
	require.NoError(t, err)

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)

	expected := `level=trace msg="GET /v1/api/healthcheck" method=RequestMiddleware.LogRequests timestamp=`
	actual, err := hook.LastEntry().String()
	require.NoError(t, err)

	assert.Contains(t, actual, expected)
}

func TestRequestMiddleware_Authorization_Success(t *testing.T) {
	password := "asdfghj"
	h, hook, authMock, err := initHandler()
	require.NoError(t, err)

	authMock.CheckAccessCalls(func(s string) (b bool, err error) {
		assert.Equal(t, password, s)
		return true, nil
	})

	req, err := http.NewRequest(http.MethodPost, "/v1/api/admin", nil)
	require.NoError(t, err)
	req.Header.Set("Authorization", password)

	okHandler := func(next http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			w.WriteHeader(http.StatusOK)
			next.ServeHTTP(w, r)
		})
	}

	handler := okHandler(h)
	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusOK, rr.Code)

	expected := `level=debug msg="authorized access" method=RequestMiddleware.Authorization timestamp=`
	actual, err := hook.LastEntry().String()
	require.NoError(t, err)

	assert.Contains(t, actual, expected)
}

func TestRequestMiddleware_Authorization_Fail(t *testing.T) {
	handler, hook, _, err := initHandler()
	require.NoError(t, err)

	req, err := http.NewRequest(http.MethodPost, "/v1/api/admin", nil)
	require.NoError(t, err)

	rr := httptest.NewRecorder()
	handler.ServeHTTP(rr, req)
	assert.Equal(t, http.StatusUnauthorized, rr.Code)

	expected := `level=debug msg="unauthorized access" method=RequestMiddleware.Authorization timestamp=`
	actual, err := hook.LastEntry().String()
	require.NoError(t, err)

	assert.Contains(t, actual, expected)
}

func initHandler() (*http.ServeMux, *test.Hook, *mocks.AuthCheckerMock, error) {
	authMock := &mocks.AuthCheckerMock{}

	logger, h := test.NewNullLogger()
	l := logrus.NewEntry(logger)
	lvl, err := logrus.ParseLevel("trace")
	if err != nil {
		return nil, nil, nil, err
	}

	l.Logger.SetLevel(lvl)
	l.Logger.Hooks.Add(h)

	reqMW := mw.NewMiddleware(authMock, l)
	mux := runtime.NewServeMux()
	handlerToTest := reqMW.SetupMiddleware(mux)

	return handlerToTest, h, authMock, nil
}
