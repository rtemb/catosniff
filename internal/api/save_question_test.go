package api_test

import (
	"context"
	"errors"
	"net/http"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/rtemb/catosniff/internal/api"
	"gitlab.com/rtemb/catosniff/internal/testing/mocks"
	proto "gitlab.com/rtemb/catosniff/pkg/proto/catosniff"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func TestHandler_SaveQuestion_Success(t *testing.T) {
	req := &proto.SaveQuestionRequest{
		Question: "What's your name?",
	}
	qServiceMock := &mocks.QuestionsServiceMock{}
	qServiceMock.SaveQuestionCalls(func(s string) error {
		assert.Equal(t, req.Question, s)
		return nil
	})
	h := api.NewHandler(qServiceMock)

	rsp, err := h.SaveQuestion(context.Background(), req)
	require.NoError(t, err)
	assert.Equal(t, http.StatusCreated, int(rsp.StateCode))
}

func TestHandler_SaveQuestion_Fail(t *testing.T) {
	req := &proto.SaveQuestionRequest{
		Question: "What's your name?",
	}
	e := errors.New("some error")
	qServiceMock := &mocks.QuestionsServiceMock{}
	qServiceMock.SaveQuestionCalls(func(s string) error {
		assert.Equal(t, req.Question, s)
		return e
	})
	h := api.NewHandler(qServiceMock)

	rsp, err := h.SaveQuestion(context.Background(), req)
	assert.Nil(t, rsp)
	assert.Error(t, err)
	assert.EqualError(t, status.Error(codes.Internal, e.Error()), err.Error())
}
