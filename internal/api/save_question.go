package api

import (
	"context"
	"net/http"

	"github.com/sirupsen/logrus"
	apiCatosniff "gitlab.com/rtemb/catosniff/pkg/proto/catosniff"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *Handler) SaveQuestion(ctx context.Context, req *apiCatosniff.SaveQuestionRequest) (*apiCatosniff.SaveQuestionResponse, error) {
	logrus.Println("SaveQuestion - 123")
	// TODO
	r := &apiCatosniff.SaveQuestionResponse{}
	err := s.questionsService.SaveQuestion(req.Question)
	if err != nil {
		r.StateCode = http.StatusInternalServerError
		return nil, status.Error(codes.Internal, err.Error())
	}

	r.StateCode = http.StatusCreated
	return r, nil
}
