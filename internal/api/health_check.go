package api

import (
	"context"

	apiCatosniff "gitlab.com/rtemb/catosniff/pkg/proto/catosniff"
)

func (s *Handler) HealthCheck(ctx context.Context, message *apiCatosniff.Empty) (*apiCatosniff.HealthCheckResponse, error) {
	r := &apiCatosniff.HealthCheckResponse{Active: true}

	return r, nil
}
