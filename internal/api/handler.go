package api

//go:generate go run github.com/maxbrunsfeld/counterfeiter/v6 --fake-name QuestionsServiceMock -o ../testing/mocks/questions_service.go . QuestionsService
type QuestionsService interface {
	SaveQuestion(q string) error
	GetQuestion() (string, error)
}

type Handler struct {
	questionsService QuestionsService
}

func NewHandler(s QuestionsService) *Handler {
	return &Handler{questionsService: s}
}
