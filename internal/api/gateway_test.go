package api_test

import (
	"context"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"os/signal"
	"strings"
	"testing"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/sirupsen/logrus"
	"github.com/stretchr/testify/suite"
	"gitlab.com/rtemb/catosniff/internal/api"
	"gitlab.com/rtemb/catosniff/internal/config"
	"gitlab.com/rtemb/catosniff/internal/testing/mocks"
	apiCatosniff "gitlab.com/rtemb/catosniff/pkg/proto/catosniff"
	"gitlab.com/rtemb/catosniff/pkg/version"
	"google.golang.org/grpc"
)

// nolint
type APITestSuite struct {
	suite.Suite
	QServiceMock *mocks.QuestionsServiceMock
	Logger       *logrus.Entry
}

func (a *APITestSuite) SetupSuite() {
	go a.initServerWithGateway()
}

func TestAPITestSuite(t *testing.T) {
	suite.Run(t, &APITestSuite{})
}

func (a *APITestSuite) TestHealthCheck() {
	resp, err := http.Get("http://localhost:8081/v1/api/healthcheck")
	a.Require().NoError(err)

	body, err := ioutil.ReadAll(resp.Body)
	a.Require().NoError(err)
	s := string(body)

	a.Equal(`{"active":true}`, s)
}

func (a *APITestSuite) TestSaveQuestion() {
	req := `{ "id": 0, "question": "What's your name?", "answer": [ { "id": 0, "answer": "Sniff" } ] }`
	rsp, err := http.Post(
		"http://localhost:8081/v1/api/admin/question",
		"application/json",
		strings.NewReader(req))
	a.Require().NoError(err)

	body, err := ioutil.ReadAll(rsp.Body)
	a.Require().NoError(err)
	s := string(body)

	a.Equal(http.StatusOK, rsp.StatusCode)
	a.Equal(`{"stateCode":201}`, s)
}

func (a APITestSuite) initServerWithGateway() {
	a.Logger = logrus.New().WithFields(logrus.Fields{
		"gitSha":  version.GitSha,
		"version": version.ServiceVersion,
		"logger":  "cmd/api-api-catosniff",
	})
	lvl, err := logrus.ParseLevel("trace")
	a.Require().NoError(err)
	a.Logger.Logger.SetLevel(lvl)

	a.QServiceMock = &mocks.QuestionsServiceMock{}
	h := api.NewHandler(a.QServiceMock)

	err = os.Setenv("ADMIN_HASH", "password")
	a.Require().NoError(err)
	cfg, err := config.Load()
	a.Require().NoError(err)
	ctx, cancel := context.WithTimeout(context.Background(), cfg.Server.GracefulShutdownTimeout)
	defer cancel()

	addr := cfg.Server.GRPCAddress + ":" + cfg.Server.GRPCPort
	lis, err := net.Listen("tcp", addr)
	a.Require().NoError(err)

	grpcServer := grpc.NewServer()
	apiCatosniff.RegisterCatOSniffAPIServiceServer(grpcServer, h)

	go func() {
		a.Logger.Info("Serving gRPC on http://", addr)
		a.Logger.Fatalln(grpcServer.Serve(lis))
	}()

	customMarshaller := &runtime.JSONPb{
		OrigName:     true,
		EmitDefaults: true,
	}
	muxOpt := runtime.WithMarshalerOption(runtime.MIMEWildcard, customMarshaller)
	gw := runtime.NewServeMux(muxOpt)

	opts := []grpc.DialOption{grpc.WithInsecure()}
	dialAddr := fmt.Sprintf("dns:///%s", addr)
	err = apiCatosniff.RegisterCatOSniffAPIServiceHandlerFromEndpoint(ctx, gw, dialAddr, opts)
	a.Require().NoError(err)

	srv := &http.Server{
		Addr:         ":" + cfg.Server.GatewayPort,
		WriteTimeout: cfg.Server.WriteTimeout,
		ReadTimeout:  cfg.Server.ReadTimeout,
		IdleTimeout:  cfg.Server.IdleTimeout,
		Handler:      gw,
	}

	go func() {
		a.Logger.Info("Serving gateway on " + cfg.Server.GatewayPort)
		a.Logger.Fatalln(srv.ListenAndServe())
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	<-c
	a.Logger.Fatalln(srv.Shutdown(ctx))
	a.Logger.Debugln("Shutting down...")
}
