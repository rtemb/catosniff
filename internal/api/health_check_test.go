package api_test

import (
	"context"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gitlab.com/rtemb/catosniff/internal/api"
	"gitlab.com/rtemb/catosniff/internal/testing/mocks"
)

func TestHandler_HealthCheck_Success(t *testing.T) {
	qServiceMock := &mocks.QuestionsServiceMock{}
	h := api.NewHandler(qServiceMock)

	ctx := context.Background()
	rsp, err := h.HealthCheck(ctx, nil)
	require.NoError(t, err)
	assert.True(t, rsp.Active)
}
