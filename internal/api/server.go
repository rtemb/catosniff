package api

import (
	"context"
	"fmt"
	"net"
	"net/http"
	"os"
	"os/signal"

	"github.com/grpc-ecosystem/grpc-gateway/runtime"
	"github.com/sirupsen/logrus"
	"gitlab.com/rtemb/catosniff/internal/config"
	"gitlab.com/rtemb/catosniff/internal/middleware"
	apiCatosniff "gitlab.com/rtemb/catosniff/pkg/proto/catosniff"
	"google.golang.org/grpc"
)

func StartGPRCWithGateway(cfg *config.Server, m *middleware.RequestMiddleware, grpcHandler *Handler, logger *logrus.Entry) {
	ctx, cancel := context.WithTimeout(context.Background(), cfg.GracefulShutdownTimeout)
	defer cancel()

	addr := cfg.GRPCAddress + ":" + cfg.GRPCPort
	lis, err := net.Listen("tcp", addr)
	if err != nil {
		logger.Fatalln("Failed to listen:", err)
	}

	grpcServer := grpc.NewServer()
	apiCatosniff.RegisterCatOSniffAPIServiceServer(grpcServer, grpcHandler)

	go func() {
		logger.Info("Serving gRPC on http://", addr)
		logger.Fatalln(grpcServer.Serve(lis))
	}()

	customMarshaller := &runtime.JSONPb{
		OrigName:     true,
		EmitDefaults: true,
	}
	muxOpt := runtime.WithMarshalerOption(runtime.MIMEWildcard, customMarshaller)
	gw := runtime.NewServeMux(muxOpt)

	opts := []grpc.DialOption{grpc.WithInsecure()}
	dialAddr := fmt.Sprintf("dns:///%s", addr)
	err = apiCatosniff.RegisterCatOSniffAPIServiceHandlerFromEndpoint(ctx, gw, dialAddr, opts)
	if err != nil {
		logger.Fatalln(err)
	}

	srv := &http.Server{
		Addr:         ":" + cfg.GatewayPort,
		WriteTimeout: cfg.WriteTimeout,
		ReadTimeout:  cfg.ReadTimeout,
		IdleTimeout:  cfg.IdleTimeout,
		Handler:      m.SetupMiddleware(gw),
	}

	go func() {
		logger.Info("Serving gateway on " + cfg.GatewayPort)
		logger.Fatalln(srv.ListenAndServe())
	}()

	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)

	<-c
	logger.Fatalln(srv.Shutdown(ctx))
	logger.Debugln("Shutting down...")
	os.Exit(0)
}
