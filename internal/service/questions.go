package service

import (
	"github.com/sirupsen/logrus"
	"gitlab.com/rtemb/catosniff/internal/storage"
)

type QuestionsService struct {
	storage storage.Storage
	logger  *logrus.Entry
}

func NewQuestionsService(s storage.Storage, l *logrus.Entry) *QuestionsService {
	return &QuestionsService{storage: s, logger: l}
}

func (s *QuestionsService) SaveQuestion(q string) error {
	return nil
}

func (s *QuestionsService) GetQuestion() (string, error) {
	return "", nil
}
