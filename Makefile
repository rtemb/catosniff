COVEROUT := ./coverage.out
APP_NAME := catosniff
OS := linux

.PHONY: deps
deps:
	$(info Installing dependencies)
	GO111MODULE=on go mod download

.PHONY: build
build:
	CGO_ENABLED=0 GOOS=$(OS) go build -a -ldflags="-X 'main.serviceVersion=$(VERSION)'" -installsuffix cgo -o main ./cmd/...

.PHONY: test
test:
	go test --cover -covermode=atomic -coverprofile=$(COVEROUT) --race -count=1 ./...

.PHONY: coveralls
coveralls:
	go get github.com/mattn/goveralls
	COVERALLS_TOKEN=wS6Dj5NDVmmyrUhg8B6IPBpVRnAdWI0OG goveralls -coverprofile=coverage.out -service=jenkins-ci -v

.PHONY: lint
lint:
	go get github.com/golangci/golangci-lint/cmd/golangci-lint
	golangci-lint -v --deadline=180s run