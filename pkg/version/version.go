package version

const (
	// nolint
	// ServiceVersion is set at build time if at all
	ServiceVersion = "unknown"

	// nolint
	// GitSha is set at build time if at all
	GitSha = "unknown"
)
