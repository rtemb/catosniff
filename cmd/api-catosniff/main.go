package main

import (
	"github.com/pkg/errors"
	"github.com/sirupsen/logrus"
	"gitlab.com/rtemb/catosniff/internal/api"
	"gitlab.com/rtemb/catosniff/internal/auth"
	"gitlab.com/rtemb/catosniff/internal/config"
	"gitlab.com/rtemb/catosniff/internal/middleware"
	"gitlab.com/rtemb/catosniff/internal/service"
	"gitlab.com/rtemb/catosniff/internal/storage"
	"gitlab.com/rtemb/catosniff/pkg/version"
)

func main() {
	logger := logrus.New().WithFields(logrus.Fields{
		"gitSha":  version.GitSha,
		"version": version.ServiceVersion,
		"logger":  "cmd/api-api-catosniff",
	})
	logger.Println("loading service configurations")
	cfg, err := config.Load()
	if err != nil {
		logger.Fatal(errors.Wrap(err, "could not load service config"))
	}

	lvl, err := logrus.ParseLevel(cfg.AppConfig.LogLevel)
	if err != nil {
		logger.Fatal(errors.Wrap(err, "could parse log level"))
	}
	logger.Logger.SetLevel(lvl)

	s := storage.NewStorage(cfg.Redis)
	simpleAuth := auth.NewSimpleAuth(cfg.AppConfig.AdminHash)
	m := middleware.NewMiddleware(simpleAuth, logger)
	qs := service.NewQuestionsService(s, logger)
	apiHandler := api.NewHandler(qs)

	api.StartGPRCWithGateway(cfg.Server, m, apiHandler, logger)
}
